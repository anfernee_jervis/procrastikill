procrastikill
=============

Limits internet usage by periodically switching off wifi.



###SYSTEM REQUIREMENTS:

Only works with Linux at the moment. Support for Windows/Mac may be released but no promises!



###SOFTWARE REQUIREMENTS:

Python is needed definitely.
tk library is needed.
rfkill must be installed for the app to work. 



###NOTES:

Its safer to run with sudo to ensure the wifi can be disabled without problems. 



###USAGE:

The script requires a file named "config.txt" within the same directory as the script. The script use that file to determine the length of each time period and also when working time ends. You can create this file, and adjust the configuration using this guide:

	
	keywords:
	workstart
	workend
	worklen
	breaklen
	lbreaklen
	gperiod


	File format:
	keyword : value


After creating/editing the file, edit line 80, "conf = parseConfig("changeme!")", to specify the location of the config file. You can now run the application with :

	python procrastikill.py

To really get the best out of this script, run with :
	
	nohup python procrastikill.py

You can add it to your binaries by doing this with the script in the current directory (insure the path to the config file is correct):

	chmod a+x procrastikill.py
	sudo mv procrastikill.py /usr/bin (or mv ~/bin/ and add an alias for it in ~/.bashrc)
	
Then run with: 
	
	nohup procrastikill.py
	
	
