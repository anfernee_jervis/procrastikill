#!/usr/bin/env python
import time
import sys
from threading import Timer
from subprocess import call
import Tkinter
import tkMessageBox

#used to exit the loop that blocks wifi
lock = 0

#FUNCTIONS

#file
def parseConfig(flocation):
	#open file
	dict = {}
	keywords = ["workstart","workend","worklen","breaklen","lbreaklen","gperiod"]
	try:
		with open(flocation,"r") as file:
			for lines in file:
				l = lines.split()
				if len(l) == 3:
					word = l[0]
					if word in keywords:
						dict[word] = int(l[2])

	except IOError:
		top = Tkinter.Tk()
		tkMessageBox.showinfo("ERROR", "Error opening configuration file")
		top.mainloop()
		sys.exit()
	
	return dict


#current hour
def currentHour():
	return int(time.strftime("%H"))


#check if its working time
def isWorkTime(currentHour, workend):
	return (workend - currentHour) > 1


#Function needs to include a statement to change the value of a variable that controls a loop executed after the timer. the loop would control the wifi 
#during work time
#work timer
def timerWork(label, function):
	lock = 1
	t = Timer(label * 60, function)
	t.start()
	call (["rfkill", "block", "wifi"])
	while lock == 1:
		time.sleep(5)
		call (["rfkill", "block", "wifi"])

#break/grace timer
def timer(label):
	time.sleep(label* 60)

#warning timer
def warning():
	#warning message
    top = Tkinter.Tk()
    tkMessageBox.showinfo("Warning!", "You have 5 minutes to prepare for work!")
    top.mainloop()
    time.sleep(300)

#function to break work timer loop
def uLock():
	lock = 0


#MAIN EXECUTION
#load config file 
conf = parseConfig("config.txt")

#begin main
timer(conf["gperiod"]) #grace period before work
warning() #warns user its time
while isWorkTime(currentHour(),conf["workend"]) == True:  # main loop
			timerWork(conf["worklen"],uLock) #working time
			timer(conf["breaklen"]) #break time
